#include <iostream>
#include <string>
#include <string.h>

using namespace std;

class Person {

private:
	char *_name;

public:

	Person(string name) {
		_name = new char[name.length()];
		strcpy(_name, name.c_str());
	}

	~Person() {
		delete _name;
	}

	const char *getName() const {
		return _name;
	}

	void print() const {
		cout << _name << endl;
	}

};

class People {

private:
	Person **_people;
	int _max;
	int _size;

public:

	People(int max) {
		_max = max;
		_size = 0;
		_people = new Person*[max];
	}

	~People() {
		for (int i = 0; i < _size; i++) {
			delete _people[i];
		}

		delete _people;
	}

	bool addPerson(const string &name) {
		if (_size == _max) {
			return false;
		}
		
		_people[_size++] = new Person(name);		
		return true;
	}

	void print() const {
		for (int i = 0; i < _size; i++) {
			_people[i]->print();
		}
	}

};

int main() {	
	People *people = new People(10);
	people->addPerson("Mahomes");
	people->addPerson("Jalen");
	people->addPerson("Travis");
	people->print();
	delete people;
	
	cout << "Program complete." << endl;
	
	return 0;
}
